import React, { Component } from 'react';
import { Button, Container, Row, Col,Dropdown,DropdownButton, Card, Alert } from 'react-bootstrap';
import DatePicker from "react-datepicker";
import {Map, InfoWindow, Marker, GoogleApiWrapper,Polyline} from 'google-maps-react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import { css } from '@emotion/core';
import { ClipLoader } from 'react-spinners';
import CSVReader from "react-csv-reader";
import Autocomplete from  'react-autocomplete';
import BarcodeReader from 'react-barcode-reader'
import {AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip, PieChart, Pie, Sector, Cell,ResponsiveContainer} from 'recharts';
import axios from 'axios';
import cors from 'cors';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import "react-datepicker/dist/react-datepicker.css";
import './App.css';

class impresionmasiva extends Component {
  constructor(props) {
    super(props);
    this.state = {
      clientes: [],
      fechaInicio: new Date(),
      fechaFin: new Date(),
      displayBuscar: 'none',
      displayFechas: 'none',
      correoCliente: '',
    };
    this.changeTituloClientes = this.changeTituloClientes.bind(this);
    this.changeFechaInicio = this.changeFechaInicio.bind(this);
    this.changeFechaFin = this.changeFechaFin.bind(this);
    this.buscar = this.buscar.bind(this);
  }

  componentWillMount() {
    const config = {
      headers: {'Content-Type': 'application/json',
        'X-API-KEY': '55IcsddHxiy2E3q653RpYtb',
        'Access-Control-Allow-Origin': '*'}
    };
    const url = document.URL;
    const urlseparada = url.split('TIPO_USUARIO=');
    const TIPO_USUARIO = urlseparada[urlseparada.length-1];
    TIPO_USUARIO = TIPO_USUARIO.replace('#', '');
    axios.post('http://app.cargoex.cl:5000/clientes', config)
        .then((res) => {
          const clientes= res.data;
          console.log('cliente de bd');
          console.log(clientes);
          const clientesBd=[];
          if (TIPO_USUARIO==='admin') {
            for (const cliente of clientes) {
              clientesBd.push(
                  {id: cliente.ID,
                    nombre: cliente.NOMBRE.toUpperCase()});
            }
          } else {
            for (const cliente of clientes) {
              if (cliente.ID+''===TIPO_USUARIO+'') {
                clientesBd.push({id: cliente.ID,
                  nombre: cliente.NOMBRE.toUpperCase()});
              }
            }
          } this.setState({clientes: clientesBd});
        });
  }
  buscar(event) {
    let dia=this.state.fechaInicio.getDate()+'';
    let mes=(this.state.fechaInicio.getMonth()+1)+'';
    if (dia.length===1) {
      dia='0'+dia;
    }
    if (mes.length===1) {
      mes='0'+mes;
    }
    let diaf = this.state.fechaFin.getDate()+'';
    let mesf=(this.state.fechaFin.getMonth()+1)+'';
    if (diaf.length===1) {
      diaf='0'+diaf;
    }
    if (mesf.length===1) {
      mesf='0'+mesf;
    }
    const fechai= this.state.fechaInicio.getFullYear()+'-'+mes+'-'+ dia;
    const fechaf=this.state.fechaFin.getFullYear()+'-'+ mesf+'-'+diaf;
    console.log(fechai);
    console.log(fechaf);
    const config = {
      headers: {'Content-Type': 'application/json',
        'X-API-KEY': '55IcsddHxiy2E3q653RpYtb',
        'Access-Control-Allow-Origin': '*'}
    };
    const data = JSON.stringify({
      fechai: fechai,
      fechaf: fechaf,
      idcliente: this.state.idcliente,
      manifiesto: this.state.manifiesto,
    });
    axios.post('http://localhost:5000/impresionmasiva', data, config)
        .then((res) => {
          const impresion= res.data;
          console.log(impresion);
          /*    this.setState({ gestiones:gestiones,
                        displayTabla:'block',
                        loading:false });  */
        });
  }
  matchStocks(state, value) {
    return (
      state.nombre.toLowerCase().indexOf(value.toLowerCase()) !== -1 );
  }
  cambiarCliente(value) {
    this.setState({
      displayBuscar: 'none',
      displayFechas: 'none',
      value: value,
    });
  }

  changeTituloClientes(item,value) {
    const config = {
      headers: {'Content-Type': 'application/json',
        'X-API-KEY': '55IcsddHxiy2E3q653RpYtb',
        'Access-Control-Allow-Origin': '*'}
    };
    const data2 = JSON.stringify({
      od: '1111',
      idcliente: value.id
    });
    axios.post("http://localhost:5000/getManifiesto",data2,config)
          .then(res => {
            var manifiesto= res.data[0].MANIFIESTO;
            this.setState({
              value:item,
              idcliente:value.id,
              displayBuscar:'block',
              displayFechas:'block',
              correoCliente:value.correo,
              manifiesto:manifiesto
            });
      });
  }

  changeFechaInicio(date) {
      if(date <= this.state.fechaFin ){
          this.setState({
            fechaInicio: date,
            displayBuscar:'block',
            displayTabla:'none'
          });
        }else{
          this.setState({

            displayBuscar:'none',
            displayTabla:'none'
          });
        }
  }
  changeFechaFin(date) {

  if(date >= this.state.fechaInicio ){
    console.log('sisas');
    this.setState({
      fechaFin: date,
      displayBuscar:'block',
      displayTabla:'none'
    });
  }else{
    this.setState({
      displayBuscar:'none',
      displayTabla:'none'
    });
    }
  }


  render() {

    return (

      <Container  >

      <Row style={{marginTop:'1%'}}>
      <Col sm={3} style={{zIndex:2,height:70, justifyContent: 'left',textAlign:"left"}}>
      <p style={{marginBottom:'0.5rem',
      color: '#202156',fontSize:20, fontWeight: 'bold',
      justifyContent: 'left',textAlign:'left'
      }}> Selecciona Cliente:
      </p>

      <Autocomplete
          value={ this.state.value }
          inputProps={{ id: 'states-autocomplete' }}
          wrapperStyle={{ position: 'relative', display: 'inline-block'}}
          items={ this.state.clientes}
          getItemValue={ item => item.nombre }
          shouldItemRender={ this.matchStocks }
          onChange={(event, value) => this.cambiarCliente(value) }
          onSelect={this.changeTituloClientes}
          renderMenu={ children => (
            <div className = "menu">
            { children }
            </div>
          )}
          renderItem={ (item, isHighlighted) => (
            <div
              className={`item ${isHighlighted ? 'item-highlighted' : ''}`}
              key={ item.id } >
              { item.nombre}
            </div>
          )}
          />

      </Col>
      <Col sm={3} style={{ justifyContent: 'left',textAlign:'left',display:this.state.displayFechas}} >
          <p style={{textAlign:"center",marginBottom:'0.5rem', color: '#202156',
          fontSize:16, fontWeight: 'bold', justifyContent: 'left',textAlign:'left'
        }}> FECHA INICIO </p>
          <DatePicker
            style={{width:'2rem'}}
            dateFormat="dd/MM/yyyy"
            selected={this.state.fechaInicio}
            onChange={this.changeFechaInicio}
            />
      </Col>
      <Col sm={3} style={{ justifyContent: 'left',textAlign:'left',display:this.state.displayFechas}} >
          <p style={{textAlign:"center",marginBottom:'0.5rem', color: '#202156',
          fontSize:16, fontWeight: 'bold', justifyContent: 'left',textAlign:'left'
        }}> FECHA FIN</p>
          <DatePicker
            dateFormat="dd/MM/yyyy"
            selected={this.state.fechaFin}
            onChange={this.changeFechaFin}
            />
      </Col>
      <Col sm={3}  >
      <Button style={{ display:this.state.displayBuscar,textAlign:'center', border: '1px solid',marginLeft:'40%', marginTop:'10%' }}
        variant="primary"
        onClick={this.buscar.bind(this)}
        >
        Buscar
        </Button>
      </Col>
      </Row>
      </Container>

    );
  }
}


export default impresionmasiva;
