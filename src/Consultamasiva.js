import React, { Component } from 'react';
import { Button, Container, Row, Col,Dropdown,DropdownButton, Card, Alert } from 'react-bootstrap';
import DatePicker from "react-datepicker";
import {Map, InfoWindow, Marker, GoogleApiWrapper,Polyline} from 'google-maps-react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import { css } from '@emotion/core';
import { ClipLoader } from 'react-spinners';
import CSVReader from "react-csv-reader";
import Autocomplete from  'react-autocomplete';
import BarcodeReader from 'react-barcode-reader'
import {AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip, PieChart, Pie, Sector, Cell,ResponsiveContainer} from 'recharts';
import axios from 'axios';
import cors from 'cors';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import "react-datepicker/dist/react-datepicker.css";
import './App.css';

class Consultamasiva extends Component {
  constructor(props){
    super(props)
    this.state = {
        fechaInicio: new Date(),
        fechaFin: new Date(),
        clientes:[],
        gestiones:[],
        idcliente:'',
        loading:false,
        displayTabla:'none',
        displayBuscar:'none',
        displayFechas:'none'

    }
    this.changeTituloClientes = this.changeTituloClientes.bind(this);
    this.buscar = this.buscar.bind(this);
    this.changeFechaInicio = this.changeFechaInicio.bind(this);
    this.changeFechaFin = this.changeFechaFin.bind(this);
  }
  buscar(event){
    this.setState({ loading: true });
    let dia=this.state.fechaInicio.getDate()+"";
    let mes=(this.state.fechaInicio.getMonth()+1)+"";
    if(dia.length===1){
      dia='0'+dia;
    }
    if(mes.length===1){
      mes='0'+mes;
    }
    let diaf = this.state.fechaFin.getDate()+"";
    let mesf=(this.state.fechaFin.getMonth()+1)+"";
    if(diaf.length===1){
      diaf='0'+diaf;
    }
    if(mesf.length===1){
      mesf='0'+mesf;
    }
    var fechai= this.state.fechaInicio.getFullYear()+"-"+mes+'-'+ dia;
    var fechaf=this.state.fechaFin.getFullYear()+"-"+ mesf+'-'+diaf;

    var config = {
    headers: {"Content-Type": "application/json","X-API-KEY": "55IcsddHxiy2E3q653RpYtb","Access-Control-Allow-Origin": "*"}
    };
    let data = JSON.stringify({
      fechai:fechai,
      fechaf:fechaf,
      idcliente:this.state.idcliente
    });
    console.log('va a llamar al servicio');
    axios.post("http://app.cargoex.cl:5000/gestiones",data,config)
      .then(res => {
        console.log('respondio servicio');
        var gestiones= res.data;
        console.log(gestiones);
        this.setState({ gestiones:gestiones,
                        displayTabla:'block',
                        loading:false });

      });
  }
  changeTituloClientes(item,value) {
    var config = {
    headers: {"Content-Type": "application/json","X-API-KEY": "55IcsddHxiy2E3q653RpYtb","Access-Control-Allow-Origin": "*"}
    };
    this.setState({
      value:item,
      idcliente:value.id,
      displayTabla:'none',
      displayFechas:'block',
      displayBuscar:'block'
    });
  }
  componentWillMount() {
    var config = {
    headers: {"Content-Type": "application/json","X-API-KEY": "55IcsddHxiy2E3q653RpYtb","Access-Control-Allow-Origin": "*"}
    };
    var url = document.URL ;
    var urlseparada = url.split('tipo_usuario=');
    var tipo_usuario=urlseparada[urlseparada.length-1];
    tipo_usuario=tipo_usuario.replace('#','');
    console.log(tipo_usuario);
    axios.post("http://app.cargoex.cl:5000/clientes",config)
      .then(res => {
        var clientes= res.data;
        console.log('cliente de bd');
        console.log(clientes);
        const clientesBd=[];

        if(tipo_usuario==='admin'){
          for(let cliente of clientes){
            clientesBd.push({id: cliente.ID,nombre: cliente.NOMBRE.toUpperCase()});
          }
        }else{
        for(let cliente of clientes){
          if(cliente.ID+''===tipo_usuario+''){
            console.log('escocgio cliente');
            console.log(cliente);
            clientesBd.push({id: cliente.ID,nombre: cliente.NOMBRE.toUpperCase()});
          }
        }
        }
        console.log(clientesBd);
        this.setState({ clientes:clientesBd });

      });

  }
  matchStocks(state, value) {
    return (

      state.nombre.toLowerCase().indexOf(value.toLowerCase()) !== -1 );
  }
  cambiarCliente(value){
    this.setState({
      displayTabla:'none',
      displayFechas:'none',
      displayBuscar:'none',
      value:value
    });
  }
  changeFechaInicio(date) {
      if(date <= this.state.fechaFin ){
          this.setState({
            fechaInicio: date,
            displayBuscar:'block',
            displayTabla:'none'
          });
        }else{
          this.setState({

            displayBuscar:'none',
            displayTabla:'none'
          });
        }
  }
  changeFechaFin(date) {

  if(date >= this.state.fechaInicio ){
    console.log('sisas');
    this.setState({
      fechaFin: date,
      displayBuscar:'block',
      displayTabla:'none'
    });
  }else{
    this.setState({
      displayBuscar:'none',
      displayTabla:'none'
    });
    }
  }
  cellButton(cell, row, enumObject, rowIndex) {
    console.log(row);
    //console.log('el objeto es'+enumObject+cell+'--'+row.nombre+'--'+rowIndex);
   let url = "http://app.cargoex.cl/clientes/public/consultaOd/"+row.OD;

     return (
        <Button
        target="_blank"
        href={url}
        variant="primary"
        >
        VER MAS
        </Button>
     )
  };
  render() {
    const selectRow = {
     mode: 'checkbox',
     showOnlySelected: true
   };
   const options = {
     toolBar: this.createCustomToolBar
   };
    return (

      <Container  >

        <Row style={{ justifyContent: 'left', marginTop:'2rem',textAlign:"left"}}>
          <Col sm={3} style={{height:40}}>
          <p style={{marginBottom:'0.5rem',
          color: '#202156',fontSize:20, fontWeight: 'bold',
          justifyContent: 'left',textAlign:'left'
          }}> Selecciona Cliente:
          </p>

          <Autocomplete
              value={ this.state.value }
              inputProps={{ id: 'states-autocomplete' }}
              wrapperStyle={{ position: 'relative', display: 'inline-block'}}
              items={ this.state.clientes}
              getItemValue={ item => item.nombre }
              shouldItemRender={ this.matchStocks }
              onChange={(event, value) => this.cambiarCliente(value) }
              onSelect={this.changeTituloClientes}
              renderMenu={ children => (
                <div className = "menu">
                { children }
                </div>
              )}
              renderItem={ (item, isHighlighted) => (
                <div
                  className={`item ${isHighlighted ? 'item-highlighted' : ''}`}
                  key={ item.id } >
                  { item.nombre}
                </div>
              )}
              />
          </Col>
          <Col sm={3} style={{ justifyContent: 'left',textAlign:'left',display:this.state.displayFechas}} >
              <p style={{textAlign:"center",marginBottom:'0.5rem', color: '#202156',
              fontSize:16, fontWeight: 'bold', justifyContent: 'left',textAlign:'left'
            }}> FECHA INICIO </p>
              <DatePicker
                style={{width:'2rem'}}
                dateFormat="dd/MM/yyyy"
                selected={this.state.fechaInicio}
                onChange={this.changeFechaInicio}
                />
          </Col>
          <Col sm={3} style={{ justifyContent: 'left',textAlign:'left',display:this.state.displayFechas}} >
              <p style={{textAlign:"center",marginBottom:'0.5rem', color: '#202156',
              fontSize:16, fontWeight: 'bold', justifyContent: 'left',textAlign:'left'
            }}> FECHA FIN</p>
              <DatePicker
                dateFormat="dd/MM/yyyy"
                selected={this.state.fechaFin}
                onChange={this.changeFechaFin}
                />
          </Col>
          <Col sm={3}  >
          <Button style={{ display:this.state.displayBuscar,textAlign:'center', border: '1px solid',marginLeft:'40%', marginTop:'10%' }}
            variant="primary"
            onClick={this.buscar.bind(this)}
            >
            Buscar
            </Button>
          </Col>
        </Row>
        <Row>
        <Col sm={12}  >
            <div style={{justifyContent: 'center',textAlign:"center"}} >
              <ClipLoader
                style={{border: '1px solid'}}
                sizeUnit={"px"}
                size={150}
                color={'#202156'}
                loading={this.state.loading}
                />
            </div>
        </Col>
        </Row>
        <Row>

        <Col sm={12}  style={{marginTop:'5%'}} >
          <p style={{
          color: '#202156',fontSize:20, fontWeight: 'bold',
          justifyContent: 'left',textAlign:'left',display:this.state.displayTabla
        }}> Gestiones : {this.state.cantidad_ordenes}
          </p>
        </Col>
        </Row>
        <Row style={{ paddingBottom:'1rem',marginTop:'2%',justifyContent: 'left',textAlign:"left",display:this.state.displayTabla}} >
          <Col sm={12} >
            <BootstrapTable
            pagination
            containerStyle={{width: '300%'}}
            data={ this.state.gestiones}
            options={ options }
            selectRow={ selectRow }
            exportCSV
            search
            >
            <TableHeaderColumn
                    width={'5%'}
                   dataField='button'
                   dataFormat={this.cellButton.bind(this)}
                 />
            <TableHeaderColumn dataField='OD' isKey={ true } dataSort={ true } width={'8%'}>OD</TableHeaderColumn>
            <TableHeaderColumn dataField='NOMBRE' dataSort={ true } width={'11%'}>NOMBRE</TableHeaderColumn>
            <TableHeaderColumn dataField='RUT' dataSort={ true } width={'11%'}>RUT</TableHeaderColumn>
            <TableHeaderColumn dataField='FECHA' dataSort={ true } width={'8%'} >FECHA</TableHeaderColumn>
            <TableHeaderColumn dataField='ESTADO_DESCRIPCION' dataSort={ true }>ESTADO</TableHeaderColumn>
            <TableHeaderColumn dataField='CERTIFICACION' dataSort={ true }>CERTIFICACION</TableHeaderColumn>
            <TableHeaderColumn dataField='LAT_TERRENO' dataSort={ true }>LATITUD</TableHeaderColumn>
            <TableHeaderColumn dataField='LONG_TERRENO' dataSort={ true }>LONGITUD</TableHeaderColumn>
            <TableHeaderColumn dataField='TIPO_CERTIFICACION' dataSort={ true }>TIPO</TableHeaderColumn>
            <TableHeaderColumn dataField='GEOREFERENCIA' dataSort={ true }>GEOREFERENCIA</TableHeaderColumn>
            <TableHeaderColumn dataField='CHOFER' dataSort={ true }>CHOFER</TableHeaderColumn>
            <TableHeaderColumn dataField='CLIENTE' dataSort={ true }>CLIENTE</TableHeaderColumn>
            <TableHeaderColumn dataField='CIUDAD' dataSort={ true }>CIUDAD</TableHeaderColumn>


            </BootstrapTable>
          </Col>
        </Row>

      </Container>

    );
  }
}


export default Consultamasiva;
