import React, { Component } from 'react';
import { Button, Container, Row, Col,Dropdown,DropdownButton, Card, Alert, InputGroup, FormControl, Modal } from 'react-bootstrap';
import DatePicker from "react-datepicker";
import {Map, InfoWindow, Marker, GoogleApiWrapper,Polyline} from 'google-maps-react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import { css } from '@emotion/core';
import { ClipLoader } from 'react-spinners';
import Autocomplete from  'react-autocomplete';
import ReactToPrint from "react-to-print";
import {AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip, PieChart, Pie, Sector, Cell,ResponsiveContainer} from 'recharts';
import axios from 'axios';
import cors from 'cors';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import "react-datepicker/dist/react-datepicker.css";
import './App.css';
import jsPDF from 'jspdf';
import Autocompleteg from "./Autocompleteg";
/* global google */

class Anden extends Component {

  constructor(props){
    super(props);
    this.state={
      tn:'',
      idcliente:'',
      correocliente:'',
      nombrecliente:'',
      displayidmanifiesto:'none',
      idmanifiesto:0,
      totalbutos:0,
      totalods:0,
      displayresultado:'none',
      flagmanifiestocargado:false,
      displaymanifiestoincompleto:'none',
      tns:[],
      tnspickeados:[],
      tnsfaltantes:[],
      modalod:false,
      modalpegar:false,
      modalpickeado:false
    };
    this.validarTn = this.validarTn.bind(this);
    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleShowPegar = this.handleShowPegar.bind(this);
    this.handleClosePegar = this.handleClosePegar.bind(this);
    this.handleShowPickeado = this.handleShowPickeado.bind(this);
    this.handleClosePickeado = this.handleClosePickeado.bind(this);
    this.cerrarManifiestoIncompleto = this.cerrarManifiestoIncompleto.bind(this);
    this.handleKeyDown = this.handleKeyDown.bind(this);

  }

  componentDidMount() {
  }
  handleKeyDown(event){

    var tn = this.state.tn;
    var flagmanifiestocargado=this.state.flagmanifiestocargado;

    if(event.key==='Enter' ){
      if(flagmanifiestocargado){
        var tns = this.state.tns;
        var tnspickeados=this.state.tnspickeados;
        var tnsfaltantes=[];
        //for para tns pickeados
        for(let i in tns){
          var aux = true;
          if(tn+''===tns[i].TN+''){
            for(let i in tnspickeados){
              if(tnspickeados[i].TN+''===tn+''){
                aux=false;
                this.setState({ modalpickeado: true });

              }
            }
            if(aux){
              tnspickeados.push(tns[i]);
            }
          }
        }
        //for para tns faltantes
        for(let i in tns){
          var aux = true;
            for(let j in tnspickeados){
              if(tnspickeados[j].TN+''===tns[i].TN+''){
                aux=false;
              //  console.log('se cumplio igualdad');
              }
            }
            if(aux){
              tnsfaltantes.push(tns[i]);
            }
        }
        if(tnspickeados.length === tns.length ){
          console.log('se cumplio igualdad');
          var idcliente = this.state.idcliente;
          var correocliente = this.state.correocliente;
          var nombrecliente = this.state.nombrecliente;
          this.cerrarManifiestoAccesoAnden(idcliente,tnspickeados,correocliente,nombrecliente,'COMPLETO');
          this.setState({
            tn:'',
            tnspickeados:[],
            tnsfaltantes:[],
            tns:[],
            displayresultado:'none',
            displaymanifiestoincompleto:'none',
            flagmanifiestocargado:false
          });
        }else{
          console.log('no se cumplio igualdad');
          this.setState({
            tn:'',
            tnsfaltantes:tnsfaltantes,
            tnspickeados:tnspickeados,
            displaymanifiestoincompleto:'block',
            displayresultado:'block'
          });
        }
      }else{
      console.log(tn);
      var config = {
      headers: {"Content-Type": "application/json","X-API-KEY": "55IcsddHxiy2E3q653RpYtb","Access-Control-Allow-Origin": "*"}
      };
      let data = JSON.stringify({
        tn: tn
      });
      axios.post("http://192.168.170.130:5000/mantn",data,config)
            .then(res => {
                console.log('react respondio validacion tn');
                const tns = res.data;
                if(tns.length>0){
                console.log(tns);
                var tnspickeados=this.state.tnspickeados;
                var tnsfaltantes=[];
                const ods = [];
                const idmanifiesto = tns[0].manifiesto;
                const idcliente = tns[0].ID_CLIENTE;
                const correo = tns[0].EMAIL_CLIENTE;
                const nombrecliente= tns[0].NOMBRE_CLIENTE;
                const bultos = tns.length;
                for(let i in tns){
                  var aux = true;
                  if(tn+''===tns[i].TN+''){
                    tnspickeados.push(tns[i]);
                  }
                  for(let j in ods){
                    if(tns[i].od===ods[j].od){
                      aux = false;
                    }
                  }
                  if (aux){
                    ods.push(tns[i]);
                  }
                }
                //for para tns faltantes
                for(let i in tns){
                  var aux = true;

                    for(let j in tnspickeados){
                      if(tnspickeados[j].TN+''===tns[i].TN+''){
                        aux=false;
                      }
                    }
                    if(aux){
                      tnsfaltantes.push(tns[i]);
                    }

                }
                if(tnspickeados.length === tns.length ){
                  this.cerrarManifiestoAccesoAnden(idcliente,tnspickeados,correo,nombrecliente,'COMPLETO');
                  this.setState({
                    tn:'',
                    idmanifiesto:idmanifiesto,
                    idcliente:idcliente,
                    correocliente:correo,
                    nombrecliente:nombrecliente,
                    totalbutos:bultos,
                    totalods:ods.length,
                    displayresultado:'none',
                    flagmanifiestocargado:false,
                    tnspickeados:[],
                    tnsfaltantes:[],
                    tns:[],
                    displaymanifiestoincompleto:'none'
                  });
                }else{
                  this.setState({
                    tn:'',
                    tnsfaltantes:tnsfaltantes,
                    idmanifiesto:idmanifiesto,
                    idcliente:idcliente,
                    correocliente:correo,
                    nombrecliente:nombrecliente,
                    totalbutos:bultos,
                    totalods:ods.length,
                    displayresultado:'block',
                    flagmanifiestocargado:true,
                    tns:tns,
                    tnspickeados:tnspickeados,
                    displaymanifiestoincompleto:'block'
                  });
                }
              }
            });
          }
    }
  }
  handleClose() {
    this.setState({ modalod: false });
  }

  handleShow() {
    this.setState({ modalod: true });
  }

  handleClosePegar() {
    this.setState({ modalpegar: false });
  }

  handleShowPegar() {
    this.setState({ modalpegar: true });
  }

  handleClosePickeado() {
    this.setState({ modalpickeado: false });
  }

  handleShowPickeado() {
    this.setState({ modalpickeado: true });
  }



  cerrarManifiestoIncompleto(){
    var idcliente = this.state.idcliente;
    var correocliente = this.state.correocliente;
    var nombrecliente = this.state.nombrecliente;
    var tnspickeados=this.state.tnspickeados;
    this.cerrarManifiestoAccesoAnden(idcliente,tnspickeados,correocliente,nombrecliente,'INCOMPLETO');
    this.setState({
      tn:'',
      tnspickeados:[],
      tnsfaltantes:[],
      tns:[],
      displayresultado:'none',
      displaymanifiestoincompleto:'none',
      flagmanifiestocargado:false
    });

  }
  cerrarManifiestoAccesoAnden(idcliente,tnspickeados,correocliente,nombrecliente,estado){
    var config = {
    headers: {"Content-Type": "application/json","X-API-KEY": "55IcsddHxiy2E3q653RpYtb","Access-Control-Allow-Origin": "*"}
    };
    let data = JSON.stringify({
      idcliente: idcliente,
      tns:tnspickeados,
      correo:correocliente,
      nombrecliente:nombrecliente,
      estado:estado
    });
    axios.post("http://192.168.170.130:5000/cerrarManifiestoAccesoAnden",data,config)
          .then(res => {
              console.log('react respondio cierre de manifiesto');
      //        const manifiestoAnden = res.data[0].MANIFIESTO;
        //      console.log(manifiestoAnden);

          });
  }
  validarTn(event){
    var tn = event.target.value;
    this.setState({
      tn:tn
    });
  }
  render() {

    return (

      <Container  >
      <Row style={{ justifyContent: 'center', marginTop:'1rem'}}>

              <Modal show={this.state.modalpickeado} onHide={this.handleClosePickeado}>
                <Modal.Header closeButton>
                  <Modal.Title>BULTO YA PICKEADO</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  <h1 style={{color:'#ff0000'}}>ESTE TN YA FUE PICKEADO</h1>
                </Modal.Body>
                <Modal.Footer>
                <Button variant="primary" onClick={this.handleClosePickeado}>
                    ok
                  </Button>
                </Modal.Footer>
              </Modal>

              <Modal show={this.state.modalod} onHide={this.handleClose}>
                <Modal.Header closeButton>
                  <Modal.Title>ODS FALTANTES</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  {this.state.tnsfaltantes.map(
                    variant => (
                                <p style={{color:'#202156'}}>   OD :&nbsp;&nbsp;&nbsp; <b style={{fontSize:20}} > {variant.od}</b> &nbsp;&nbsp;&nbsp; TN :  &nbsp;&nbsp;&nbsp; <b style={{fontSize:20}}>{variant.TN} </b>  </p>

                        ))}
                </Modal.Body>
                <Modal.Footer>
                <Button variant="primary" onClick={this.handleClose}>
                    ok
                  </Button>
                </Modal.Footer>
              </Modal>

              <Modal show={this.state.modalpegar} onHide={this.handleClosePegar}>
                <Modal.Header closeButton>
                  <Modal.Title>ACCION PROHIBIDA</Modal.Title>
                </Modal.Header>
                <Modal.Body>

                  <p style={{color:'#202156',textAlign:"justify"}}>  PROHIBIDO COPIAR DESDE TECLADO Y MOUSE !!!! </p>

                </Modal.Body>
                <Modal.Footer>
                <Button variant="primary" onClick={this.handleClosePegar}>
                    ok
                  </Button>
                </Modal.Footer>
              </Modal>


        <Col sm={12} style={{ height:60, justifyContent: 'center',textAlign:"center"}}>
            <h1 style={{textAlign:"center", color: '#202156',  fontWeight: 700
          }}> MODULO DE PICKING ACCESO ANDEN </h1>
        </Col>
      </Row>
      <Row style={{ justifyContent: 'center', marginTop:'2rem',marginBottom:'1rem'}}>
          <Col sm={3} style={{zIndex:2,height:70, justifyContent: 'left',textAlign:"left"}}>
          <p style={{marginBottom:'0.5rem',
          color: '#202156',fontSize:20, fontWeight: 'bold',
          justifyContent: 'left',textAlign:'left',display:this.state.displayod
          }}> Pickea Numero:
          </p>
          <input
          id="numero"
          type="text"
          name="name"
          autocomplete="off"
          onPaste={this.handleShowPegar}
          value={this.state.tn}
          onChange={this.validarTn}
          onKeyDown={this.handleKeyDown}
          />
          </Col>
          <Col sm={2} style={{zIndex:2,height:70, justifyContent: 'left',textAlign:"left"}}>
            <p style={{marginBottom:'0.5rem',
            color: '#202156',fontSize:20, fontWeight: 'bold',
            justifyContent: 'left',textAlign:'left',display:this.state.displayresultado
            }}> ID de manifiesto:
            </p>
            <p style={{marginBottom:'0.5rem',
            color: '#202156',fontSize:20, fontWeight: 'bold',
            justifyContent: 'left',textAlign:'left',display:this.state.displayresultado
          }}> {this.state.idmanifiesto}
            </p>
          </Col>
          <Col sm={2} style={{zIndex:2,height:70, justifyContent: 'left',textAlign:"left"}}>
            <p style={{marginBottom:'0.5rem',
            color: '#202156',fontSize:20, fontWeight: 'bold',
            justifyContent: 'left',textAlign:'left',display:this.state.displayresultado
          }}> Total bultos:
            </p>
            <p style={{marginBottom:'0.5rem',
            color: '#202156',fontSize:20, fontWeight: 'bold',
            justifyContent: 'left',textAlign:'left',display:this.state.displayresultado
          }}> {this.state.totalbutos}
            </p>
          </Col>
          <Col sm={2} style={{zIndex:2,height:70, justifyContent: 'left',textAlign:"left"}}>
            <p style={{marginBottom:'0.5rem',
            color: '#202156',fontSize:20, fontWeight: 'bold',
            justifyContent: 'left',textAlign:'left',display:this.state.displayresultado
          }}> Total ods:
            </p>
            <p style={{marginBottom:'0.5rem',
            color: '#202156',fontSize:20, fontWeight: 'bold',
            justifyContent: 'left',textAlign:'left',display:this.state.displayresultado
          }}> {this.state.totalods}
            </p>
          </Col>
          <Col sm={2} style={{zIndex:2,height:70}}>
            <p style={{marginBottom:'0.5rem',
            color: '#202156',fontSize:20, fontWeight: 'bold',
            justifyContent: 'left',textAlign:'left',display:this.state.displayresultado
          }}> Bultos Pickeados:
            </p>
            <Button style={{display:this.state.displayresultado, justifyContent: 'center',textAlign:'center'}}
                variant="primary"
                onClick={this.handleShow}
            >
                {this.state.tnspickeados.length} / {this.state.tns.length}
            </Button>

          </Col>
        </Row>
        {this.state.tnspickeados.map(
          variant => (
                <Row style={{ display: 'block' ,border: '1px solid',borderColor: '#202156'}}>
                  <Col sm={12} style={{ justifyContent: 'left',textAlign:"left"}}>
                      <p style={{ marginTop:'1rem',color:'#202156'}}>   OD :&nbsp;&nbsp;&nbsp; <b style={{fontSize:20}} > {variant.OD}</b> &nbsp;&nbsp;&nbsp; TN :  &nbsp;&nbsp;&nbsp; <b style={{fontSize:20}}>{variant.TN} </b>  &nbsp;&nbsp;&nbsp;  BULTOS : {variant.BULTOS} </p>

                  </Col>

                </Row>
              ))}
          <Row >
                <Col sm={12} style={{ justifyContent: 'center',textAlign:"center",marginTop:'1rem',display:this.state.displaymanifiestoincompleto}} >
                  <Button
                      variant="danger"
                      onClick={this.cerrarManifiestoIncompleto}
                  >
                    Finalizar Manifiesto Incompleto
                  </Button>
                  </Col>
          </Row>
      </Container>
    );
  }
}

export default Anden;
